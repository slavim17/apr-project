#!/bin/bash
LIB="./lib/"
mkdir -p ./tmp
export TRACER="./tmp/trace_log.out"
LD_PRELOAD=${LIB}libmt.so $@

./analyzer $TRACER
rm -rf ./tmp
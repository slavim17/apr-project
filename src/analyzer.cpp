#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stdio.h>
#include <unistd.h>
#include <vector>

#include <fstream>

using namespace std;

streampos position = 0;

struct Entry
{
    char type;
    size_t size = 0;
    size_t addressIn = 0;
    size_t addressOut = 0;
    size_t caller = 0;
    double time;
    string backtrace;
};

size_t parseAddress(const string &input)
{
    if (input == "(nil)")
    {
        return 0;
    }
    else
    {
        size_t addr;
        istringstream ss(input);
        ss >> std::dec >> addr;
        return addr;
    }
}

Entry parseLine(const string &line)
{
    Entry z;

    stringstream sstream(line);
    sstream >> z.type;

    // M [pointerOut] [sizeGain] [time] [callerFuncPointer] [backtrace]
    // A // Todo--n'xD
    // R [pointerIn] [pointerOut] [sizeNew] [time] [callerFuncPointer] [backtrace]
    // F [pointerIn] [time] [callerFuncPointer] [backtrace]
    string buff;
    switch (z.type)
    {
    case 'M':
        sstream >> buff;
        z.addressOut = parseAddress(buff);
        sstream >> std::dec >> z.size;
        sstream >> std::dec >> z.time;
        sstream >> buff;
        z.caller = parseAddress(buff);
        getline(sstream, z.backtrace);
        break;

    case 'F':
        sstream >> buff;
        z.addressIn = parseAddress(buff);
        sstream >> std::dec >> z.time;
        sstream >> buff;
        z.caller = parseAddress(buff);
        getline(sstream, z.backtrace);
        break;

    case 'R':
        sstream >> buff;
        z.addressIn = parseAddress(buff);
        sstream >> buff;
        z.addressOut = parseAddress(buff);
        sstream >> std::dec >> z.size;
        sstream >> std::dec >> z.time;
        sstream >> buff;
        z.caller = parseAddress(buff);
        getline(sstream, z.backtrace);
        break;

    default:
        throw "wtf";
    }

    return z;
}

vector<Entry> readNew(const string &filename)
{
    vector<Entry> result;

    ifstream file;
    file.open(filename.c_str(), ios::in);

    string buff;

    file.seekg(position, file.beg);
    while ((getline(file, buff)))
    {
        position = file.tellg();
        result.push_back(parseLine(buff));
    }
    file.close();
    return result;
}

int main(int argc, char const *argv[])
{
    // M [pointerOut] [sizeGain] [time] [callerFuncPointer] [backtrace]
    // R [pointerIn] [pointerOut] [sizeNew] [time] [callerFuncPointer] [backtrace]
    // F [pointerOut] [time] [callerFuncPointer] [backtrace] 

    bool rtAnalysis = false;
    if (argc > 2 && string(argv[2]) == "-rt")
    {
        rtAnalysis = true;
    }

    if (argc < 1)
    {
        printf("need file to analyze\n");
        exit(1);
    }

    vector<Entry /*zaznam*/> dataRaw = readNew(string(argv[1]));
    //analyze

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    int maxTime = 0;
    if (dataRaw.size() > 0)
    {
        maxTime = (int)((dataRaw.end() - 1)->time);
    }
    maxTime++;

    map<int, map<int /*address*/, size_t /*size*/>> allocTable;
    map<int, map<size_t /*caller*/, int /*count*/>> callerFreqTable;

    //***************CALLER TABLE Fill******************
    map<int, map<size_t /*caller*/, vector<Entry>>> caller_table;
    map<int, map<char, map<size_t /*caller*/, vector<Entry>>>> typeCallerTable;

    for (const auto &z : dataRaw)
    {
        for (int i = (int)z.time; i < maxTime; i++)
        {
            caller_table[i][z.caller].push_back(z);
        }

        for (int i = (int)z.time; i < maxTime; i++)
        {
            typeCallerTable[i][z.type][z.caller].push_back(z);
        }
    }

    /////////////////CALLER TABLE Fill*****************//

    int TIME = 0;

    //minmax
    map<int, vector<pair<double /*cas*/, int /*zmena*/>>> minmax;
    //total allocated, free
    map<int, size_t> totalAlloc;
    map<int, size_t> totalFree;

    map<int, size_t> maxAlloc;

    map<int, size_t> actualAllocated;
    size_t prevAllocSize;
    map<int, int> numberF, numberM, numberR;

    for (const auto &z : dataRaw)
    {
        if (TIME != (int)z.time)
        {

            for (size_t i = TIME; i < z.time; i++)
            {
                totalFree[i] = totalFree[TIME];
                totalAlloc[i] = totalAlloc[TIME];
                maxAlloc[i] = maxAlloc[TIME];
                actualAllocated[i] = actualAllocated[TIME];
                numberM[i] = numberM[TIME];
                numberR[i] = numberR[TIME];
                numberF[i] = numberF[TIME];
                minmax[i] = minmax[TIME];
                callerFreqTable[i] = callerFreqTable[TIME];
                allocTable[i] = allocTable[TIME];
            }
        }

        TIME = (int)z.time;
        prevAllocSize = 0;

        switch (z.type)
        {
        case 'M':
            if (z.addressOut == 0)
                break;
            numberM[TIME]++;
            allocTable[TIME].insert(make_pair(z.addressOut, z.size));
            minmax[TIME].push_back(make_pair(z.time, z.size));
            actualAllocated[TIME] += z.size;
            totalAlloc[TIME] += z.size;
            if (callerFreqTable[TIME].find(z.caller) == callerFreqTable[TIME].end())
            {
                callerFreqTable[TIME].insert(make_pair(z.caller, 1));
            }
            else
            {
                callerFreqTable[TIME].at(z.caller)++;
            }

            break;
        case 'F':
            if (z.addressIn == 0)
                break;
            try
            {
                prevAllocSize = allocTable[TIME].at(z.addressIn);
            }
            catch (...)
            {
                cerr << "undefined free" << endl;
            }
            numberF[TIME]++;
            allocTable[TIME].erase(z.addressIn);
            minmax[TIME].push_back(make_pair(z.time, -prevAllocSize));
            actualAllocated[TIME] -= prevAllocSize;
            totalFree[TIME] += prevAllocSize;

            if (callerFreqTable[TIME].find(z.caller) == callerFreqTable[TIME].end())
            {
                callerFreqTable[TIME].insert(make_pair(z.caller, 1));
            }
            else
            {
                callerFreqTable[TIME].at(z.caller)++;
            }

            break;
        case 'R':
            if (z.addressOut == 0)
                break;
            numberR[TIME]++;
            if (z.addressIn != 0)
            { //realloc reallocing
                try
                {
                    prevAllocSize = allocTable[TIME].at(z.addressIn);
                    allocTable.erase(z.addressIn);
                }
                catch (...)
                {
                    cerr << "undefined realloc" << endl;
                    break;
                }
                allocTable[TIME].insert(make_pair(z.addressOut, z.size));
                minmax[TIME].push_back(make_pair(z.time, prevAllocSize - z.size));
                totalAlloc[TIME] += z.size;
                totalFree[TIME] += prevAllocSize;
                actualAllocated[TIME] += z.size - prevAllocSize; // TODO check
            }
            else
            { // same malloc
                allocTable[TIME].insert(make_pair(z.addressOut, z.size));
                minmax[TIME].push_back(make_pair(z.time, z.size));
                actualAllocated[TIME] += z.size;
                totalAlloc[TIME] += z.size;
            }

            if (callerFreqTable[TIME].find(z.caller) == callerFreqTable[TIME].end())
            {
                callerFreqTable[TIME].insert(make_pair(z.caller, 1));
            }
            else
            {
                callerFreqTable[TIME].at(z.caller)++;
            }

            break;
        default:
            throw "wtf";
        }
        if (actualAllocated > maxAlloc)
        {
            maxAlloc = actualAllocated;
        }
    }

    map<int, multimap<int, size_t>> sortedCallerTable;
    for (int t = 0; t < maxTime; t++)
    {
        for (auto it : callerFreqTable[t])
        {
            sortedCallerTable[t].insert(make_pair(it.second, it.first));
        }
    }
    int t;
    rtAnalysis ? t = 0 : t = maxTime - 1;
    
    for (; t < maxTime; t++)
    {
        if (rtAnalysis)
        {
            cout << "\x1B[2J\x1B[H"; //screen wiping
            cout << "max time= " << maxTime << "s" << endl;
            cout << "current time= " << t << "s" << endl;
        }
        int callerNum = 5;
        cout << "first " << callerNum << " callers out of " << caller_table[t].size() << " tmp: " << (int)sortedCallerTable[t].size() << endl;
        auto iterator = sortedCallerTable[t].rbegin();
        for (int i = 0; i < min(5, (int)sortedCallerTable[t].size()); ++i)
        {
            cout << "caller from " << caller_table[t].at(iterator->second).begin()->backtrace << " with " << iterator->first << " calls of "
                 << caller_table[t].at(iterator->second).begin()->type << endl;
            iterator++;
        }

        cout << "total allocated: " << totalAlloc[t] << endl;
        cout << "total freed: " << totalFree[t] << endl;
        cout << "unfreed: " << totalAlloc[t] - totalFree[t] << endl;
        cout << "Maximum at a moment allocated: " << maxAlloc[t] << endl;

        cout << "Malloc called " << numberM[t] << endl;
        cout << "Realloc called " << numberR[t] << endl;
        cout << "Free called " << numberF[t] << endl;

        cout << endl
             << "found unique syscalls: " << endl;
        cout << "Free:    " << typeCallerTable[t]['F'].size() << endl;
        cout << "Malloc:  " << typeCallerTable[t]['M'].size() << endl;
        cout << "Realloc: " << typeCallerTable[t]['R'].size() << endl;

        sleep(1.1);
    }

    return 0;
}

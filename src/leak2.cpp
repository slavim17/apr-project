#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

void leak(int num)
{
    fprintf(stderr, "%s allocating %d bytes...\n", __PRETTY_FUNCTION__, num);
    {
        new int[num];

    }
}

int main(int argc, char** argv)
{
    srand((unsigned)time(NULL));
    fprintf(stderr, "%s: PID: %d\n", __PRETTY_FUNCTION__, getpid());
    /*
    while (1) {
        leak(rand() % 1024);
	sleep(rand() % 10);
    }*/
    return 0;
}
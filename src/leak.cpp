#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
           #include <mcheck.h>

void leak(int num)
{
    fprintf(stderr, "%s allocating %d bytes...\n", __PRETTY_FUNCTION__, num);
    malloc(num);
}

void leak22(int num)
{
    fprintf(stderr, "%s allocating 222222%d bytes...\n", __PRETTY_FUNCTION__, num);
    malloc(num);
}

int main(int argc, char** argv)
{
    mtrace();
    srand((unsigned)time(NULL));
    fprintf(stderr, "%s: PID: %d\n", __PRETTY_FUNCTION__, getpid());
    
    int i = 0;
    while (i < 3) {
        leak(rand() % 1024+128);
        leak22(rand() % 128);
	    sleep(rand() % 3);
        i++;
    }
    return 0;
}
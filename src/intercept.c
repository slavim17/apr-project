#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include <sys/time.h>

/* Prototypes for our hooks.  */
static void my_init_hook(void);
static void *my_malloc_hook(size_t, const void*);
static void *my_realloc_hook(void*, size_t, const void*);
static void my_free_hook(void*, const void*);
 
/* Variables to save original hooks. */
static void *(*old_malloc_hook)(size_t, const void *);
static void *(*old_realloc_hook)(void*, size_t, const void*);
static void  (*old_free_hook)(void*, const void *);
 
/* Override initializing hook from the C library. */
void (*__malloc_initialize_hook) (void) = my_init_hook;

static struct timeval  tv1;
static pthread_mutex_t lock;


static FILE * openLogFile(const char * mode){
    char * filenamEnv =  getenv("TRACER");
    
    if(!filenamEnv){
        
        int pid = getpid();
        // int enoughForPID  = (int)((ceil(log10(pid))+1)*sizeof(char));
        char pidArr[7];// = malloc(enoughForPID);
        sprintf(pidArr, "%d", pid);
        int pidArrLen = strlen(pidArr);
        const char * name = "mem_analyze_";
        int nameLen = strlen(name);
        filenamEnv = malloc(nameLen + pidArrLen + 1);
        memcpy(filenamEnv, name, nameLen );
        memcpy(filenamEnv + nameLen, pidArr, pidArrLen +1 );
        // free(pidArr);
    }

    // fprintf(stderr, "openning %s\n", filenamEnv );
    FILE * result = fopen(filenamEnv, mode);
    if(!filenamEnv){
        free(filenamEnv);
    }

    
    return result;
}
static void my_init_hook(void)
{

    if (pthread_mutex_init(&lock, PTHREAD_MUTEX_NORMAL) != 0) {
        printf("\n mutex init has failed\n");
        exit(1);
    }
    gettimeofday(&tv1, NULL);

    pthread_mutex_lock(&lock);
    FILE * file  = openLogFile("w");
    fclose(file);
    pthread_mutex_unlock(&lock);

    old_malloc_hook = __malloc_hook;
    old_realloc_hook = __realloc_hook;
    old_free_hook   = __free_hook;
    __malloc_hook   = my_malloc_hook;
    __free_hook     = my_free_hook;
    __realloc_hook  = my_realloc_hook;
}
 
static void restoreOldHooks()
{
    __malloc_hook = old_malloc_hook;
    __realloc_hook = old_realloc_hook;
    __free_hook = old_free_hook;
}
 
static void restoreMyHooks()
{
    __malloc_hook = my_malloc_hook;
    __realloc_hook = my_realloc_hook;
    __free_hook = my_free_hook;
}
 
static void saveOldHooks()
{
    old_malloc_hook = __malloc_hook;
    old_realloc_hook = __realloc_hook;
    old_free_hook = __free_hook;
}
 
static double calcTime() {
    struct timeval tv2;
    gettimeofday(&tv2, NULL);
    double elapsed = (tv2.tv_usec - tv1.tv_usec)/1000000.0  + (double) (tv2.tv_sec - tv1.tv_sec);
    return elapsed;
}

size_t backtrace(void **,size_t );
char**  backtrace_symbols(void **, size_t);

char** get_backtrace(size_t maxstacklen, size_t offset, size_t *act_size) {
    void* stack[maxstacklen + offset];
    size_t size = backtrace(stack, maxstacklen + offset);

    *act_size = 0;

    if (size <= offset) {
        return NULL;
    }

    *act_size = size - offset;
    return backtrace_symbols(&stack[offset], size - offset);
}



//---------------------------------HOOKS---------------------------------

static void* my_malloc_hook(size_t size, const void* caller)
{
    void* result;
    pthread_mutex_lock(&lock);
    // Restore all old hooks
    restoreOldHooks();
 
    // Call recursively
    result = malloc(size);
 

    FILE * file  = openLogFile("a");
    
    double elapsed = calcTime();


    char * backtrace;
    size_t bsize;
    char ** back = get_backtrace(1, 2,&bsize );
    if(bsize > 0 && back){
        backtrace = back[0];
        
    }else{
        backtrace = "NULL";
    }


    // fprintf(stderr, "M %ld %u %lf %ld %s\n", (long int)result, (unsigned int) size,  elapsed, (long int) caller, backtrace);
    fprintf(file, "M %ld %u %lf %ld %s\n", (long int)result, (unsigned int) size,  elapsed, (long int) caller, backtrace);




    fclose(file);
 
    // Save underlying hooks
    saveOldHooks();
    // Restore our own hooks
    restoreMyHooks();
    pthread_mutex_unlock(&lock);

    return result;
}

static void* my_realloc_hook(void*  ptr, size_t size, const void* caller) {
    void* result;
    pthread_mutex_lock(&lock);

    // Restore all old hooks
    restoreOldHooks();
 
    // Call realloc() recursively.
    result = realloc( ptr, size);
 
    FILE * file  = openLogFile("a");

    double elapsed = calcTime();


    char * backtrace ;
    size_t bsize;
    char ** back = get_backtrace(1, 2,&bsize );
    if(bsize > 0 && back){
        backtrace = back[0];
        
    }else{
        backtrace = "NULL";
    }

    // fprintf(stderr, "R %ld %ld %u %lf %ld %s\n", (long int) ptr, (long int)result, (unsigned int) size, elapsed, (long int) caller, backtrace);
    fprintf(file, "R %ld %ld %u %lf %ld %s\n", (long int) ptr, (long int)result, (unsigned int) size, elapsed, (long int) caller, backtrace);
    fclose(file);

    saveOldHooks();
    // Restore our own hooks
    restoreMyHooks();
    pthread_mutex_unlock(&lock);

    return result;
}

static void my_free_hook(void* ptr, const void*  caller)
{
    pthread_mutex_lock(&lock);
    restoreOldHooks();

    FILE * file  = openLogFile("a");
    double elapsed = calcTime();

    char * backtrace;

    size_t bsize;
    char ** back = get_backtrace(1, 2,&bsize );
    if(bsize > 0 && back){
        backtrace = back[0];
        
    }else{
        backtrace = "NULL";
    }

    // fprintf(stderr, "F %ld %lf %ld %s\n", (long int) ptr, elapsed, (long int) caller, backtrace);
    fprintf(file, "F %ld %lf %ld %s\n", (long int) ptr, elapsed, (long int) caller, backtrace);

    fclose(file);
    free (ptr);
    
    saveOldHooks();
    restoreMyHooks();
    pthread_mutex_unlock(&lock);

}
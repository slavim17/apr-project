#include <cstdlib>
#include <cstdio>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <memory.h>

class X{
    public:

    X(int size):size_(size){
        array = (int*)malloc(size* sizeof(int));
    }

    X(const X & x){
        size_ = x.size_;
        array = (int*)malloc(x.size_);
    }

    X(X && x){
        size_ = x.size_;
        array = (x.array);
    }

    X& operator=(const X & x){
        size_ = x.size_;
        array = (int* )realloc(array, x.size_);
        // memcpy(array, x.array, x.size_);
        return *this;
    }

    ~X(){
        free(array);
    }

    private:
    int size_;
    int * array;  

};


int main(int argc, char** argv)
{
    srand((unsigned)time(NULL));
    fprintf(stderr, "%s: PID: %d\n", __PRETTY_FUNCTION__, getpid());
    
    int i = 0;
    while (i < 3) {
        auto a = new X(10);
        // a = new X(20);
        sleep(rand() % 2);
        delete a;
        i++;
	    sleep(rand() % 4);
    }
    return 0;
}
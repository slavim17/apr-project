PP=g++
CC=gcc
CCFLAGS=-Wall -Wno-deprecated-declarations -O0 -g
OBJ=./obj/
LIB=./lib/
SRC=./src/

all:$(LIB)libmt.so analyzer

$(OBJ)intercept.o: $(SRC)intercept.c
	mkdir -p $(OBJ)
	$(CC) -fPIC -c -o $@ $< $(CCFLAGS) 

$(LIB)libmt.so: $(OBJ)intercept.o
	mkdir -p $(LIB)
	$(CC) -shared -o $@ $<

C: $(LIB)libmt.so

run: 
	./run.sh $(ARGS)

clear:
	rm -rf $(OBJ) $(LIB)

analyzer: $(SRC)analyzer.cpp
	$(PP) -o $@ $< $(CCFLAGS) 

